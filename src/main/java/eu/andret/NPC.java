package eu.andret;

import java.util.Objects;

public class NPC extends LivingEntity {
	private String role;

	public NPC(final String role) {
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	public void setRole(final String role) {
		this.role = role;
	}

	@Override
	public void speak(final String text) {
		System.out.println(text);
	}

	@Override
	public String toString() {
		return "NPC{" +
				"role='" + role + '\'' +
				"} " + super.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final NPC npc)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return Objects.equals(getRole(), npc.getRole());
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getRole());
	}
}

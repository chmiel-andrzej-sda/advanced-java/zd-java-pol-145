package eu.andret;

public record Location(int x, int y) {
}

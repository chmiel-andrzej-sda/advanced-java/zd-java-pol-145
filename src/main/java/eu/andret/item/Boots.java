package eu.andret.item;

import eu.andret.Material;

public final class Boots extends ArmorItem {
	public Boots(final String name, final String description, final int value, final int armor, final Material material) {
		super(name, description, value, armor, material);
	}

	@Override
	public String getSound() {
		return "Tip toe";
	}

	@Override
	public String toString() {
		return "Boots{} " + super.toString();
	}
}

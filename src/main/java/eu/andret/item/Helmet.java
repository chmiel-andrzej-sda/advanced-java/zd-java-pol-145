package eu.andret.item;

import eu.andret.Material;

public final class Helmet extends ArmorItem {
	public Helmet(final String name, final String description, final int value, final int armor, final Material material) {
		super(name, description, value, armor, material);
	}

	@Override
	public String getSound() {
		return "Knock-knock";
	}

	@Override
	public String toString() {
		return "Helmet{} " + super.toString();
	}
}

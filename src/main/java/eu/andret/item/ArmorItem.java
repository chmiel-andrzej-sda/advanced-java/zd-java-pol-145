package eu.andret.item;


import eu.andret.Material;

import java.util.Objects;

public abstract sealed class ArmorItem extends Item permits Helmet, Chestplate, Leggins, Boots {
	private final NestedCalculation nestedCalculation = new NestedCalculation();
	private final InnerCalculation innerCalculation = new InnerCalculation();
	private int armor;
	private Material material;

	protected ArmorItem(final String name, final String description, final int value, final int armor, final Material material) {
		super(name, description, value);
		this.armor = armor;
		this.material = material;
	}

	public int getArmor() {
		return armor;
	}

	public void setArmor(final int armor) {
		if (armor >= 0 && armor <= 100) {
			value = innerCalculation.calculate();
			this.armor = armor;
		}
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(final Material material) {
		this.material = material;
	}

	@Override
	public void setValue(final int value) {
		super.setValue(innerCalculation.calculate());
		innerCalculation.realValue = value;
	}

	@Override
	public String toString() {
		return "ArmorItem{" +
				"armor=" + armor +
				", material=" + material +
				"} " + super.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final ArmorItem armorItem)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return getArmor() == armorItem.getArmor()
				&& getMaterial() == armorItem.getMaterial();
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getArmor(), getMaterial());
	}

	private static class NestedCalculation {
		private int realValue;

		public int calculate(final int armor) {
			return (int) (realValue * armor / 100.);
		}
	}

	private class InnerCalculation {
		private int realValue;

		public int calculate() {
			return (int) (realValue * armor / 100.);
		}
	}
}

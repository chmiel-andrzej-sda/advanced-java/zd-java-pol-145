package eu.andret.item;

import eu.andret.Material;

public final class Chestplate extends ArmorItem {
	public Chestplate(final String name, final String description, final int value, final int armor, final Material material) {
		super(name, description, value, armor, material);
	}

	@Override
	public String getSound() {
		return "Bim-bam";
	}

	@Override
	public String toString() {
		return "Chestplate{} " + super.toString();
	}
}

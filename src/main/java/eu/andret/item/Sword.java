package eu.andret.item;

import eu.andret.Material;

import java.util.Objects;

public non-sealed class Sword extends Item {
	private Material material;

	public Sword(final String name, final String description, final int value, final Material material) {
		super(name, description, value);
		this.material = material;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(final Material material) {
		this.material = material;
	}

	@Override
	public String getSound() {
		return "cling";
	}

	@Override
	public String toString() {
		return "Sword{" +
				"material=" + material +
				"} " + super.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Sword sword)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return getMaterial() == sword.getMaterial();
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getMaterial());
	}
}

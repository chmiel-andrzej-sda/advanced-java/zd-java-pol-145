package eu.andret.item;

import eu.andret.Material;

public final class Leggins extends ArmorItem {
	public Leggins(final String name, final String description, final int value, final int armor, final Material material) {
		super(name, description, value, armor, material);
	}

	@Override
	public String getSound() {
		return "Scratch-scratch";
	}

	@Override
	public String toString() {
		return "Leggins{} " + super.toString();
	}
}

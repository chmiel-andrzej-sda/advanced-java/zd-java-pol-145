package eu.andret.practice.exercise17;

@FunctionalInterface
public interface Converter {
	float convert(float value);
}

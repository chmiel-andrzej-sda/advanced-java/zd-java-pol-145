package eu.andret.practice.exercise26;

public record Car(String name, String description, CarType carType) {
}

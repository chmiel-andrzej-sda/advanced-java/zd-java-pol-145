package eu.andret.practice.exercise26;

import java.util.List;

public record Model(String name, int productionStartYear, List<Car> cars) {
}

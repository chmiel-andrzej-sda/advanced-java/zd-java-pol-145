package eu.andret.practice.exercise26;

public enum CarType {
    COUPE,
    CABRIO,
    SEDAN,
    HATCHBACK
}

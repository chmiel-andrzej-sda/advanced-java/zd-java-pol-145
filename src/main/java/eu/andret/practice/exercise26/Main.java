package eu.andret.practice.exercise26;

import java.util.Collection;
import java.util.List;

public final class Main {
	public static void main(final String[] args) {
		final List<Manufacturer> manufacturers = List.of(
				new Manufacturer("Manufacturer1", 1950, List.of(
						new Model("Dodge", 1914, List.of(
								new Car("Charger", "My dream car.", CarType.SEDAN),
								new Car("Challenger", "Not bad.", CarType.CABRIO)
						)),
						new Model("Chevrolet", 2020, List.of(
								new Car("Camaro", "My second dream car.", CarType.CABRIO),
								new Car("Corvette", "Well maybe.", CarType.COUPE)
						))
				)),
				new Manufacturer("Manufacturer2", 1988, List.of(
						new Model("BMW", 2023, List.of(
								new Car("X3", "Ahh BMW", CarType.SEDAN),
								new Car("X7", "Yup.", CarType.COUPE)
						)),
						new Model("Ford", 2008, List.of(
								new Car("Focus", "I have this one.", CarType.HATCHBACK),
								new Car("Fiesta", "YAAAS", CarType.CABRIO)
						))
				)));

		System.out.println("5. Lista wszystkich nazw modeli");
		manufacturers.stream()
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.map(Model::name)
				.forEach(System.out::println);


		System.out.println("6. Lista startu produkcji");
		manufacturers.stream()
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.map(Model::productionStartYear)
				.forEach(System.out::println);


		System.out.println("7. Lista wszystkich nazw aut");
		manufacturers.stream()
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.map(Model::cars)
				.flatMap(Collection::stream)
				.map(Car::name)
				.forEach(System.out::println);

		System.out.println("8. Lista wszystkich opisów aut");
		manufacturers.stream()
				.map(Manufacturer::models)
				.flatMap(Collection::stream)
				.map(Model::cars)
				.flatMap(Collection::stream)
				.map(Car::description)
				.forEach(System.out::println);

		// 1 2
		// 3 4

		// 1 2 3 4
	}
}

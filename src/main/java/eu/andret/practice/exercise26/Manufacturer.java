package eu.andret.practice.exercise26;

import java.util.List;

public record Manufacturer(String name, int yearOfCreation, List<Model> models) {
}

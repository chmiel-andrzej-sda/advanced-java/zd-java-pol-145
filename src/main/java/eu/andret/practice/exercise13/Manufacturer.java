package eu.andret.practice.exercise13;

public record Manufacturer(String name, int yearOfEstablishment, String country) {
}

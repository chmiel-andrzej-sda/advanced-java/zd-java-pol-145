package eu.andret.practice.exercise27;

public final class Main {
	public static void main(final String[] args) {
		final Joiner<Integer> integerJoiner = new Joiner<>("#");
		final String join = integerJoiner.join(1, 2, 3, 4, 5);
		System.out.println(join);
	}
}

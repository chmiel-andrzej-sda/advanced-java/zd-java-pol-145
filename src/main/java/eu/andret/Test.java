package eu.andret;

public class Test {
	@Priority(10)
	public String publicTest() {
		return "Hello!";
	}

	@Priority(value = 1, description = "protected")
	protected void protectedTest() {

	}

	@Priority(5)
	int test() {
		return -1;
	}

	private boolean privateTest() {
		return false;
	}
}

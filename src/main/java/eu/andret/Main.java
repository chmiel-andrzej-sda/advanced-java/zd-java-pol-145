package eu.andret;

import eu.andret.item.ArmorItem;
import eu.andret.item.Helmet;
import eu.andret.item.Item;
import eu.andret.item.Sword;
import eu.andret.person.Address;
import eu.andret.person.Box;
import eu.andret.person.Gender;
import eu.andret.person.MyException;
import eu.andret.person.MyRuntimeException;
import eu.andret.person.Person;
import eu.andret.person.Student;
import eu.andret.person.Teacher;
import eu.andret.person.Textable;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.IntSupplier;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.IntStream;

public class Main {
	private static final Scanner SCANNER = new Scanner(System.in);

	public static void main(final String[] args) throws IOException, ExecutionException, InterruptedException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException, InstantiationException {
		final Address address = new Address("Kolorowa", "Kraków", "PL");

		final Person person = new Teacher("Andrzej", "Chmiel", address, Gender.MALE, "Math");
		final Student student = new Student("Test", "Testowy", null, Gender.UNSPECIFIED, 1234);
		System.out.println(student.getLastName());

		final LivingEntity entity = new NPC("");

		if (entity instanceof final Player player) {
			System.out.println(player.getLevel());
		}

		final Item item = new Helmet("Magic sword", "", 100, 20, Material.IRON);

		entity.getInventory().add(item);

		System.out.println(person.getFirstName());

		final Teacher t1 = new Teacher("", "", null, Gender.NON_BINARY, "");
		final Teacher t2 = new Teacher("", "", null, Gender.FEMALE, "");
		if (t1.equals(t2)) {
			System.out.println("same");
		} else {
			System.out.println("not same");
		}
		System.out.println(t1);

		final Movable[] movables = {
				new NPC("Vendor"),
				new Player()
		};
		Arrays.stream(movables)
				.map(Movable::getLocation)
				.forEach(System.out::println);

		final Textable textable = text -> System.out.println(text + text);
		final Textable textable1 = System.out::println;
		textable.write("abc");
		textable1.write("");

		final Student student1 = new Student("", "", null, Gender.MALE, 1) {
			@Override
			public int getIndexNumber() {
				return 0;
			}
		};
		System.out.println(student1.getIndexNumber());

		final Speakable speakable = text -> System.out.println("I'm anonymous: " + text);
		speakable.speak("Hello!");

		final Player player = new Player();
		final LivingEntity[] entities = {
				player,
				new NPC("Villager"),
				new LivingEntity() {
					@Override
					public void speak(final String text) {
						System.out.println("anonymous entity: " + text);
					}
				}
		};
		for (final LivingEntity entity1 : entities) {
			entity1.speak("the loop");
		}

		final LivingEntity[] livingEntities = new LivingEntity[10];
		for (int i = 0; i < 10; i++) {
			final int count = i;
			livingEntities[i] = new LivingEntity() {
				@Override
				public void speak(final String text) {
					System.out.println(count + ", " + text);
				}
			};
		}

		for (final LivingEntity livingEntity : livingEntities) {
			livingEntity.speak("the last but not least");
		}
		System.out.println(Gender.MALE.getPl());
		System.out.println(student.getGender().getPl());

		final Box<Teacher> box = new Box<>(t1);
		System.out.println(box.value());
		print(entity);

		final Item item1 = new Helmet("", "", 1, 1, Material.IRON);
		player.getInventory().add(item1);
		player.getInventory()
				.getItems()
				.stream()
				.filter(Objects::nonNull)
				.map(Item::getMetadata)
				.filter(Predicate.not(Map::isEmpty))
				.map(Map::values)
				.flatMap(Collection::stream)
				.map(Metadatum::getValue)
				.forEach(metadatum -> {
					System.out.println(metadatum);
					System.out.println(metadatum.getClass().getName());
				});
		for (int i = 0; i < 36; i++) {
			final Item stringItem = player.getInventory().getItems().get(0);
			if (stringItem != null) {
				final Map<Class<?>, Metadatum<?>> metadata = stringItem.getMetadata();
				if (!metadata.isEmpty()) {
					for (final Map.Entry<Class<?>, Metadatum<?>> entry : metadata.entrySet()) {
						final Metadatum<?> metadatum = entry.getValue();
						System.out.println(metadatum.getValue());
						System.out.println(metadatum.getValue().getClass().getName());
					}
				}
			}
		}
		speak(new Animal());

		final Person.Pet pet = person.new Pet("Rex");
		final Box<Integer> box1 = new Box<>(1);
		System.out.println(box1.value());
		System.out.println(box1);
		final Item stringItem = player.getInventory().getItems().get(0);
		stringItem.add(Integer.class, new Metadatum<>(1));
		if (stringItem != null) {
			System.out.println(stringItem.getName());
		}
//        System.out.println(player.getHelmet().getName());
//        System.out.println(player.getInventory().getItem(-100));
		try {
			throwsIllegalArgumentException();
		} catch (final IllegalArgumentException e) {
			e.printStackTrace();
		}
		try {
			throwsReflectiveOperationException();
		} catch (final ReflectiveOperationException e) {
			e.printStackTrace();
		}
		try {
			throwsMyRuntimeException();
		} catch (final MyRuntimeException e) {
			e.printStackTrace();
		}
		try {
			throwsMyException();
		} catch (final MyException e) {
			e.printStackTrace();
		}
//        readInt();
//        readInt2();

		final List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(0);
		list.add(null);
		System.out.println(list);
		list.stream()
				.filter(Objects::nonNull)
				.map(i -> i + 1)
				.forEach(System.out::println);
		System.out.println(list.isEmpty());
		System.out.println(list.size());
		System.out.println(list.contains(7));
		System.out.println(list.contains(2));
		list.remove(Integer.valueOf(2));
		System.out.println(list);
		list.clear();
		System.out.println(list);

		final Set<String> set = new HashSet<>();
		set.add("Hello!");
		set.add("World!");
		set.add("World!");
		set.add("World!");
		System.out.println(set);
		System.out.println(set.contains("World!"));
		System.out.println(set.isEmpty());
		set.remove("Hello!");
		System.out.println(set);

		final Set<Entity> entitySet = new TreeSet<>(new Comparator<Entity>() {
			@Override
			public int compare(final Entity o1, final Entity o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		System.out.println(entitySet);

		final Set<Player> playerHashSet = new HashSet<>();
		final Set<Player> playerTreeSet = new TreeSet<>(new Comparator<Player>() {
			@Override
			public int compare(final Player o1, final Player o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		final Player player1 = new Player();
		player1.setName("Rob");
		final Player player2 = new Player();
		player2.setName("Tom");
		player2.setLocation(new Location(1, 1));
		playerHashSet.add(player1);
		playerHashSet.add(player1);
		playerHashSet.add(player2);
		playerHashSet.add(player2);
		playerTreeSet.add(player1);
		playerTreeSet.add(player1);
		playerTreeSet.add(player2);
		playerTreeSet.add(player2);
		for (final Player player3 : playerTreeSet) {
			System.out.println(player3.getName());
		}
		for (final Player player3 : playerHashSet) {
			System.out.println(player3.getName());
		}

		final List<Entity> entityList = List.of(
				new NPC("Vendor"),
				player1,
				player1,
				player1
		);
		final List<Entity> entities1 = new ArrayList<>(new HashSet<>(entityList));
		System.out.println(entities1);
		final Map<String, Entity> map = new HashMap<>();
		map.put("x", player1);
		map.put("y", player1);
		map.put("z", player1);
		map.put("w", player1);
		System.out.println(map);
		for (final Map.Entry<String, Entity> entry : map.entrySet()) {
			System.out.println(entry.getKey());
		}

		final Function<Player, Inventory> playerInventoryFunction = LivingEntity::getInventory;
		System.out.println(playerInventoryFunction.apply(player1));

		final Consumer<Item> itemConsumer = item2 -> System.out.println(item2.getName());
		itemConsumer.accept(item1);

		final Supplier<NPC> playerSupplier = () -> new NPC("Test");
		System.out.println(playerSupplier.get());

		final Predicate<Inventory> inventoryPredicate = inventory ->
				inventory.getItems().isEmpty();

		System.out.println(inventoryPredicate.test(player1.getInventory()));

		final IntUnaryOperator function = n -> {
			int result = 0;
			for (int i = 0; i <= n; i++) {
				result += i;
			}
			return result;
		};
		System.out.println(function.applyAsInt(100));

		final Random random = new Random();
		final IntSupplier randomSupplier = () -> random.nextInt(11);
		System.out.println(randomSupplier.getAsInt());

		final Consumer<String> stringConsumer = string -> {
			for (final char c : string.toCharArray()) {
				System.out.println(c);
			}
		};
		stringConsumer.accept("abc");

		final IntPredicate isPrime = n -> {
			for (int i = 2; i < n / 2; i++) {
				if (n % i == 0) {
					return false;
				}
			}
			return true;
		};
		System.out.println(isPrime.test(8));
		System.out.println(isPrime.test(7));


		IntStream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
				.filter(i -> i % 2 == 0)
				.average()
				.ifPresent(System.out::println);

		player.getInventory()
				.getItems()
				.stream()
				.map(Item::getName)
				.forEach(System.out::println);

		player.getInventory()
				.getItems()
				.stream()
				.filter(it -> it.getMetadata().isEmpty())
				.forEach(System.out::println);


		// Supplier<Integer> => IntSupplier
		// Stream<Integer> => IntStream
		final int sum = player.getInventory()
				.getItems()
				.stream()
				.mapToInt(Item::getValue)
				.sum();
		System.out.println(sum);

		playerTreeSet.stream()
				.filter(p -> p.getHelmet() == null ||
						p.getChestplate() == null ||
						p.getLeggins() == null ||
						p.getBoots() == null)
				.forEach(System.out::println);
		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.map(itemList -> itemList.get(0))
				.ifPresent(System.out::println);

		System.out.println(Optional.of(player)
				.map(Player::getHelmet)
				.map(ArmorItem::getMaterial)
				.orElse(null));

//        System.out.println(Optional.of(player)
//                .map(Player::getGold)
//                .filter(x -> x != 0)
//                .orElseThrow(IllegalArgumentException::new));

		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.map(itemList -> itemList.stream()
						.mapToInt(Item::getValue)
						.average()
						.orElse(0))
				.filter(i -> i != 0)
				.ifPresent(System.out::println);
		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.map(itemList -> itemList.stream()
						.map(Item::getMetadata)
						.mapToInt(Map::size)
						.sum())
				.ifPresent(System.out::println);

		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.map(itemList -> itemList.stream()
						.filter(item2 -> !item2.getMetadata().isEmpty())
						.toList())
				.ifPresent(System.out::println);

		final File file = new File(".");
		final File[] files = file.listFiles();
		System.out.println(Arrays.toString(files));
		Arrays.stream(files)
				.filter(f -> !f.getName().startsWith("."))
				.forEach(System.out::println);

		final Path path = Paths.get("pom.xml");
		Files.readAllLines(path)
				.forEach(System.out::println);

		try (final BufferedReader reader = new BufferedReader(new FileReader("pom.xml"))) {
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		} catch (final IOException ex) {
			ex.printStackTrace();
		}

		Files.createDirectories(Paths.get("files"));
		final Path path1 = Paths.get("files", "file1.txt");
		try (final PrintWriter printWriter = new PrintWriter(path1.toFile())) {
			printWriter.println("Hello!");
		}

		final Path path2 = Paths.get("files", "file2.txt");
		try (final BufferedWriter writer = new BufferedWriter(new FileWriter(path2.toFile()))) {
			writer.write("Hello!\n");
		}

		final Player filePlayer1 = new Player();
		filePlayer1.setName("Andret");
		filePlayer1.setGold(200);
		filePlayer1.setHealth(50);

		final Player filePlayer2 = new Player();
		filePlayer2.setName("deyanix");
		filePlayer2.setGold(100);
		filePlayer2.setHealth(100);

		final Path playersPath = Paths.get("players");
		Files.createDirectories(playersPath);
		for (final Player p : List.of(filePlayer1, filePlayer2)) {
			final Path playerPath = Paths.get("players", String.format("%s.txt", p.getName()));
			try (final BufferedWriter writer = new BufferedWriter(new FileWriter(playerPath.toFile()))) {
				writer.write(p.toString());
				writer.write('\n');
			}
		}

		final File players = playersPath.toFile();
		Arrays.stream(players.listFiles())
				.forEach(System.out::println);
		System.out.println(read("Andret"));
		System.out.println(read("test"));

		final Thread helloThread1 = new Thread(() -> System.out.println("hello world 1"));
		final Thread helloThread2 = new Thread(() -> System.out.println("hello world 2"));
		helloThread1.start();
		helloThread2.start();

		final Thread testThread1 = new Thread(() -> {
			for (int i = 1; i < 21; i++) {
				System.out.println(i);
				try {
					Thread.sleep(100);
				} catch (final InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		});
		final Thread testThread2 = new Thread(() -> {
			for (int i = 0; i < 10; i++) {
				System.out.println(random.nextInt(101));
				try {
					Thread.sleep(200);
				} catch (final InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		});
		final Thread testThread3 = new Thread(() -> {
			final Scanner scanner = new Scanner(System.in);
			final int i = scanner.nextInt();
			System.out.println(i * i);
			System.out.println(i * i * i);
		});
//        testThread1.start();
//        testThread2.start();
//        testThread3.start();

		final Thread sameThread1 = getSameThread(450);
		final Thread sameThread2 = getSameThread(500);
		final Thread sameThread3 = getSameThread(550);

//        sameThread1.start();
//        sameThread2.start();
//        sameThread3.start();

		final MyResource myResource = new MyResource(); // x = 0
		final Thread resourceThread1 = new Thread(() -> {
			try {
				myResource.setX(2);
				System.out.println("1: " + myResource.getX());
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		});
		final Thread resourceThread2 = new Thread(() -> {
			try {
				myResource.setX(3);
				System.out.println("2: " + myResource.getX());
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		});

//        resourceThread1.start();
//        resourceThread2.start();

		final MyListResource myListResource = new MyListResource();
		final Thread listThread1 = new Thread(() -> {
			for (int i = 1; i < 11; i++) {
				try {
					myListResource.add(i);
				} catch (final InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		});

		final Thread listThread2 = new Thread(() -> {
			for (int i = 1; i < 11; i++) {
				try {
					System.out.println(myListResource.getList());
				} catch (final InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		});

		listThread1.start();
		listThread2.start();

		final ExecutorService service = Executors.newFixedThreadPool(10);
		final Future<Integer> submit = service.submit(() -> {
			try {
				Thread.sleep(200);
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			return 1;
		});
		System.out.println(submit.get());
		System.out.println(" ==== REFLECT ====");
		final Class<Person> personClass = Person.class;
		final Method[] declaredMethods = personClass.getDeclaredMethods();
		Arrays.stream(declaredMethods)
				.map(Method::getName)
				.forEach(System.out::println);
		final Method privateTest = personClass.getDeclaredMethod("privateTest", int.class);
		privateTest.setAccessible(true);
		System.out.println(privateTest.invoke(person, 1));
		// person.privateTest(1);
		System.out.println(privateTest.getReturnType());
		System.out.println(privateTest.getModifiers());

		final Class<Test> testClass = Test.class;
		final Test test = new Test();
		final Method[] methods = testClass.getDeclaredMethods();
		for (final Method method : methods) {
			method.setAccessible(true);
			System.out.println(method.invoke(test));
			System.out.println(method.getReturnType());
		}

		final Field firstName = personClass.getDeclaredField("firstName");
		firstName.setAccessible(true);
		System.out.println(firstName.get(person));
		firstName.set(person, "broken");
		System.out.println(firstName.get(person));
		System.out.println(person.getFirstName());
		reset(person);
		System.out.println(person);
		reset(student);
		System.out.println(student);

		final Class<Student> studentClass = Student.class;
		final Constructor<Student> constructor = studentClass
				.getDeclaredConstructor(String.class, String.class, Address.class, Gender.class, int.class);
		final Student instance = constructor.newInstance("Name", "Surname", null, Gender.MALE, 0);
		System.out.println(instance);

		final Class<Sword> swordClass = Sword.class;
		final Constructor<Sword> declaredConstructor = swordClass.getDeclaredConstructor(String.class, String.class, int.class, Material.class);
		final Sword sword = declaredConstructor.newInstance("test1", "test2", 0, Material.BRONZE);
		System.out.println(sword);

//        Class<LivingEntity> entityClass = LivingEntity.class;
//        Constructor<LivingEntity> entityConstructor = entityClass.getDeclaredConstructor();
//        LivingEntity living = entityConstructor.newInstance();
//        System.out.println(living);
		final Method[] methods1 = MyListResource.class.getDeclaredMethods();
		Arrays.stream(methods1)
				.filter(m -> m.isAnnotationPresent(MyAnnotation.class))
				.forEach(m -> {
					final MyAnnotation annotation = m.getAnnotation(MyAnnotation.class);
					System.out.println(annotation.test() + ", " + annotation.value());
					System.out.println(m.getName() + ", " + m.getReturnType());
				});

		Arrays.stream(methods)
				.sorted((o1, o2) -> {
					final int a1 = Optional.of(o1)
							.map(x -> x.getAnnotation(Priority.class))
							.map(Priority::value)
							.orElse(0);
					final int a2 = Optional.of(o2)
							.map(x -> x.getAnnotation(Priority.class))
							.map(Priority::value)
							.orElse(0);
					return Integer.compare(a1, a2);
				}).forEach(method -> {
					try {
						method.setAccessible(true);
						System.out.println(method.invoke(test));
						Optional.of(method)
								.map(x -> x.getAnnotation(Priority.class))
								.map(Priority::description)
								.ifPresent(System.out::println);
					} catch (final ReflectiveOperationException e) {
						throw new RuntimeException(e);
					}
				});
		for (final Method method : methods) {
			method.setAccessible(true);
			System.out.println(method.invoke(test));
			System.out.println(method.getReturnType());
		}
	}

	public static void reset(final Person person) throws IllegalAccessException {
		final Class<? extends Person> personClass = person.getClass();
		final Field[] fields = personClass.getDeclaredFields();
		for (final Field field : fields) {
			field.setAccessible(true);
			if (field.getType().isAssignableFrom(boolean.class)) {
				field.set(person, false);
			} else if (field.getType().isPrimitive()) {
				field.set(person, 0);
			} else {
				field.set(person, null);
			}
		}
	}

	private static Thread getSameThread(final int millis) {
		return new Thread(() -> {
			for (int i = 0; i < 11; i++) {
				System.out.println(Thread.currentThread().getName() + ", " + i);
				try {
					Thread.sleep(millis);
				} catch (final InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		});
	}

	public static String read(final String playerName) throws IOException {
		final Path path = Paths.get("players", String.format("%s.txt", playerName));
		if (!Files.exists(path)) {
			return null;
		}
		try (final BufferedReader reader = new BufferedReader(new FileReader(path.toFile()))) {
			return reader.readLine();
		}
	}

	public static void throwsIllegalArgumentException() {
		throw new IllegalArgumentException();
	}

	public static void throwsReflectiveOperationException() throws ReflectiveOperationException {
		throw new ReflectiveOperationException();
	}

	public static void throwsMyRuntimeException() {
		throw new MyRuntimeException("Yay my exception!");
	}

	public static void throwsMyException() throws MyException {
		throw new MyException("Yay my exception!");
	}

	public static int readInt() {
		while (true) {
			final String line = SCANNER.nextLine();
			try {
				return Integer.parseInt(line);
			} catch (final NumberFormatException e) {
				System.out.println("It's not an int!");
			}
		}
	}

	public static int readInt2() {
		do {
			if (SCANNER.hasNextInt()) {
				return SCANNER.nextInt();
			}
			SCANNER.nextLine();
			System.out.println("It's not an int!");
		} while (true);
	}

	public static <T extends Entity> void print(final T o) {
		System.out.println(o.getName());
	}

	public static <S extends Speakable> void speak(final S speakable) {
		speakable.speak("test");
	}
}

class NoCtor {
	public NoCtor() {
		throw new UnsupportedOperationException();
	}
}

class MyResource {
	private int x;

	public synchronized int getX() throws InterruptedException {
		Thread.sleep(1000);
		return x;
	}

	public synchronized void setX(final int x) throws InterruptedException {
		Thread.sleep(1000);
		this.x = x;
	}
}

class MyListResource {
	private final List<Integer> list = new ArrayList<>();

	public synchronized void add(final int x) throws InterruptedException {
		Thread.sleep(100);
		list.add(x);
	}

	@MyAnnotation(value = "", test = 1)
	public synchronized List<Integer> getList() throws InterruptedException {
		Thread.sleep(100);
		return list;
	}
}

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnnotation {
	String value();

	int test() default 0;
}

package eu.andret;

import eu.andret.item.Boots;
import eu.andret.item.Chestplate;
import eu.andret.item.Helmet;
import eu.andret.item.Item;
import eu.andret.item.Leggins;

import java.util.Objects;

public class Player extends LivingEntity implements Jumpable {
	private int level;
	private int gold;
	private Item itemInHand;
	private Helmet helmet;
	private Chestplate chestplate;
	private Leggins leggins;
	private Boots boots;

	public int getLevel() {
		return level;
	}

	public void setLevel(final int level) {
		if (level > 0) {
			this.level = level;
		}
	}

	public int getGold() {
		return gold;
	}

	public void setGold(final int gold) {
		if (level > 0) {
			this.gold = gold;
		}
	}

	public Item getItemInHand() {
		return itemInHand;
	}

	public void setItemInHand(final Item itemInHand) {
		this.itemInHand = itemInHand;
	}

	public Helmet getHelmet() {
		return helmet;
	}

	public void setHelmet(final Helmet helmet) {
		this.helmet = helmet;
	}

	public Chestplate getChestplate() {
		return chestplate;
	}

	public void setChestplate(final Chestplate chestplate) {
		this.chestplate = chestplate;
	}

	public Leggins getLeggins() {
		return leggins;
	}

	public void setLeggins(final Leggins leggins) {
		this.leggins = leggins;
	}

	public Boots getBoots() {
		return boots;
	}

	public void setBoots(final Boots boots) {
		this.boots = boots;
	}

	@Override
	public void speak(final String text) {
		System.out.println(getName() + ": " + text);
	}

	@Override
	public String toString() {
		return "Player{" +
				"level=" + level +
				", gold=" + gold +
				", itemInHand=" + itemInHand +
				", helmet=" + helmet +
				", chestplate=" + chestplate +
				", leggins=" + leggins +
				", boots=" + boots +
				"} " + super.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Player player)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return getLevel() == player.getLevel()
				&& getGold() == player.getGold()
				&& Objects.equals(getItemInHand(), player.getItemInHand())
				&& Objects.equals(getHelmet(), player.getHelmet())
				&& Objects.equals(getChestplate(), player.getChestplate())
				&& Objects.equals(getLeggins(), player.getLeggins())
				&& Objects.equals(getBoots(), player.getBoots());
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getLevel(), getGold(), getItemInHand(), getHelmet(), getChestplate(), getLeggins(), getBoots());
	}

	@Override
	public void jump() {
		System.out.println(getName() + " is jumping!");
	}
}

package eu.andret.person;

@FunctionalInterface
public interface Textable {
	void write(String text);
}

package eu.andret.person;

public record Box<T>(T value) {
}

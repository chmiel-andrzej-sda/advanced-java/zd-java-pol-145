package eu.andret.person;

public class MyRuntimeException extends RuntimeException {
	public MyRuntimeException(final String message) {
		super(message);
	}
}

package eu.andret.person;

import java.util.Objects;

public non-sealed class Student extends Person implements Textable {
	private int indexNumber;
	private boolean testBoolean;

	public Student(final String firstName, final String lastName, final Address address, final Gender gender, final int indexNumber) {
		super(firstName, lastName, address, gender);
		this.indexNumber = indexNumber;
	}

	public int getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(final int indexNumber) {
		this.indexNumber = indexNumber;
	}

	@Override
	public void doJob() {
		System.out.println("I'm studying");
	}

	@Override
	public String toString() {
		return "Student{" +
				"indexNumber=" + indexNumber +
				"} " + super.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Student student)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return getIndexNumber() == student.getIndexNumber();
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getIndexNumber());
	}

	@Override
	public void write(final String text) {

	}
}

package eu.andret.person;

import java.util.Objects;

public abstract sealed class Person permits Student, Teacher {
	private String firstName;
	private String lastName;
	private Address address;
	private Gender gender;

	protected Person(final String firstName, final String lastName, final Address address, final Gender gender) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.gender = gender;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(final Address address) {
		this.address = address;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(final Gender gender) {
		this.gender = gender;
	}

	public abstract void doJob();

	@Override
	public String toString() {
		return "Person{" +
				"firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", address=" + address +
				'}';
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Person person)) {
			return false;
		}
		return Objects.equals(getFirstName(), person.getFirstName())
				&& Objects.equals(getLastName(), person.getLastName())
				&& Objects.equals(getAddress(), person.getAddress());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getFirstName(), getLastName(), getAddress());
	}

	private void privateTest() {
	}

	private void privateTest(final int x) {
	}

	protected void protectedTest() {
	}

	void test() {
	}

	public class Pet {
		private final String name;
		private Gender gender;

		public Pet(final String name) {
			this.name = name;
			System.out.println(Person.this.gender.toString());
		}
	}
}

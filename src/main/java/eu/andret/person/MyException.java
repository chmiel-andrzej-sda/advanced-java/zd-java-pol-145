package eu.andret.person;

public class MyException extends Exception {
	public MyException(final String message) {
		super(message);
	}
}

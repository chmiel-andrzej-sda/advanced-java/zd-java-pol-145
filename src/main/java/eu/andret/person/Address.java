package eu.andret.person;

import java.util.Objects;

public class Address {
	private String street;
	private String city;
	private String country;

	public Address(final String street, final String city, final String country) {
		this.street = street;
		this.city = city;
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(final String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Address{" +
				"street='" + street + '\'' +
				", city='" + city + '\'' +
				", country='" + country + '\'' +
				'}';
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Address address)) {
			return false;
		}
		return Objects.equals(getStreet(), address.getStreet())
				&& Objects.equals(getCity(), address.getCity())
				&& Objects.equals(getCountry(), address.getCountry());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getStreet(), getCity(), getCountry());
	}
}

package eu.andret.homework01;

public record Point2D(int x, int y, char name) {
}

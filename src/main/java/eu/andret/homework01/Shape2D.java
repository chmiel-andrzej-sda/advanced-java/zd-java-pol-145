package eu.andret.homework01;

public class Shape2D extends Polyline {
	public Shape2D(final int size) {
		super(size);
	}

	@Override
	protected boolean isCorrect() {
		if (!lines[0].start().equals(lines[lines.length - 1].end())) {
			return false;
		}
		return super.isCorrect();
	}

	public void check() {
		final double length = lines[0].length();
		for (final Line line : lines) {
			if (line.length() != length) {
				System.out.println("Undefined length");
				return;
			}
		}
		System.out.println("Success " + length);
	}
}

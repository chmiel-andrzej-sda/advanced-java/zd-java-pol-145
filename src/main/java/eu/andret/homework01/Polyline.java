package eu.andret.homework01;

public class Polyline {
	protected final Line[] lines;

	public Polyline(final int size) {
		lines = new Line[size];
	}

	public Line[] getLines() {
		return lines;
	}

	public double getLength() {
		if (!isCorrect()) {
			return -1;
		}
		double result = 0;
		for (final Line line : lines) {
			result += line.length();
		}
		return result;
	}

	protected boolean isCorrect() {
		for (int i = 0; i < lines.length - 1; i++) {
			final Line line = lines[i];
			final Line next = lines[i + 1];
			if (!line.end().equals(next.start())) {
				return false;
			}
		}
		return true;
	}
}

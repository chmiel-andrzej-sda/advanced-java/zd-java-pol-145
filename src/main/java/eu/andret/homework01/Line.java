package eu.andret.homework01;

public record Line(Point2D start, Point2D end) {
	public double length() {
		final double diffX = start.x() - end.x();
		final double diffY = start.y() - end.y();
		return Math.sqrt(diffX * diffX + diffY * diffY);
	}
}

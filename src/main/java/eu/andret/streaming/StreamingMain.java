package eu.andret.streaming;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;

public final class StreamingMain {
	public static void main(final String[] args) {
		final Supplier<String> stringSupplier = () -> "Test String";
		System.out.println(stringSupplier.get());
		System.out.println(stringSupplier.get());
		System.out.println(stringSupplier.get());
		System.out.println(stringSupplier.get());

		final Consumer<String> stringConsumer = s -> System.out.println(s + "!");
		stringConsumer.accept("Hello");
		stringConsumer.accept(stringSupplier.get());

		final ToIntFunction<String> stringIntegerFunction = String::length;
		System.out.println(stringIntegerFunction.applyAsInt("hello"));
		System.out.println(stringIntegerFunction.applyAsInt(stringSupplier.get()));

		final Predicate<String> notEmptyPredicate = Predicate.not(String::isEmpty);
		System.out.println(notEmptyPredicate.test("x"));
		System.out.println(notEmptyPredicate.test(""));

		final List<Integer> collect = IntStream.of(2, 8, 5, 3, 1, 9, 7, -4)
				.filter(i -> i % 2 == 0)
				.map(i -> i * i)
				.sorted()
				.boxed()
				.toList();
		System.out.println(collect);

		final Test test = null;

		final String name = getString(test);
		System.out.println(name);

		final String name2 = Optional.ofNullable(test)
				.map(Test::getBox)
				.map(Box::getName)
				.orElse("");
		System.out.println(name2);
	}

	private static String getString(final Test test) {
		if (test == null) {
			return "";
		}
		final Box box = test.getBox();
		if (box == null) {
			return "";
		}
		final String name = box.getName();
		if (name == null) {
			return "";
		}
		return name;
	}
}

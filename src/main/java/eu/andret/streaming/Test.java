package eu.andret.streaming;

import java.util.Objects;

public class Test {
	private Box box;

	public Test() {
		this(null);
	}

	public Test(final Box box) {
		this.box = box;
	}

	public Box getBox() {
		return box;
	}

	public void setName(final Box box) {
		this.box = box;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Test test = (Test) o;
		return Objects.equals(box, test.box);
	}

	@Override
	public int hashCode() {
		return Objects.hash(box);
	}

	@Override
	public String toString() {
		return "Test{" +
				"box='" + box + '\'' +
				'}';
	}
}

class Box {
	private String name;

	public Box() {
		this(null);
	}

	public Box(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Box test = (Box) o;
		return Objects.equals(name, test.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public String toString() {
		return "Test{" +
				"name='" + name + '\'' +
				'}';
	}
}

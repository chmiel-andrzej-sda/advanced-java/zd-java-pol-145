package eu.andret;

public interface Movable {
	Location getLocation();

	void setLocation(Location location);
}
